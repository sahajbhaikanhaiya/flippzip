Pod::Spec.new do |s|

  s.name             = 'FlippFramework'

  s.version          = '0.0.1'

  s.summary          = 'FlippFramework Framework Summary'

  s.description      = "FlippFramework Framework Description"

  s.homepage         = 'https://github.com'
  
  s.license          = { :type => 'MIT', :file => 'LICENSE' }

  s.author           = { 'sahajbhaikanhaiya' => 'sahaj.bhaikanhaiya@gmail.com' }

  s.source           = { :http => 'https://bitbucket.org/sahajbhaikanhaiya/flippzip/raw/3027a3687d5c14abcb90b66d658fe1128a5e3893/FlippFramework.zip',:flatten => true }
  
  s.platform = :ios
  
  s.ios.deployment_target = '9.0'

  s.frameworks = 'FlippFramework'

  s.preserve_paths = 'FlippFramework.framework'

  s.vendored_frameworks = 'FlippFramework.framework'
  
end